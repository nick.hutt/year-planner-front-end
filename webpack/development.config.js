const webpack = require('webpack');
const { merge } = require('webpack-merge');
const commonConfig = require('./common.config.js');

module.exports = merge(commonConfig, {
    devtool: 'inline-source-map',
    mode: 'development',
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development'),
            'process.env.YEAR_PLANNER_BACKEND_URL': JSON.stringify('http://localhost:8090/api'),
            'process.env.GOOGLE_CLIENT_ID': JSON.stringify('108059476810-nfhlk0s06qstlvs4hh5j29os03scjoib.apps.googleusercontent.com'),
            'process.env.GOOGLE_REDIRECT_URI': JSON.stringify('http://localhost:8090/')
       })
    ],
    optimization: {
        minimize: false
    },
});