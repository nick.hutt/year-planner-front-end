const path = require('path'),
    webpack = require('webpack');
      
module.exports = {
    entry: [
        './src/index.js'
    ],
    output: {
        path: path.join(__dirname, '../public'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-react', '@babel/preset-env'],
                plugins: [
                    "@babel/plugin-transform-class-properties" //, { "loose": true }
                    // ['@babel/plugin-proposal-private-property-in-object', { loose: true }]
                ]
            }
        },
        {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
                'file-loader'
            ]
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    devServer: {
        historyApiFallback: true,
        static: {
            directory: path.join(__dirname, "../public")
        },
        port: 8090,
        proxy: [
            {
                context: ['/api'],
                target: 'http://localhost:13080',
                pathRewrite: { '^/api': '' },
            },
        ]
    }
};