# Year Planner Front End

## Prerequisite

* Docker
* npm https://nodejs.org/en/download/

## Building

To build the project:

```
> npm ci
> npm run build
```

## Running

Add a `docker-compose-override.yml` with the following info:

```
version: "3"
services:
  year-planner:
    environment:
      GOOGLE-ACCESS-TOKEN_CLIENT-ID: <client-id>
      GOOGLE-ACCESS-TOKEN_CLIENT-SECRET: <secret>
      GOOGLE-ACCESS-TOKEN_REDIRECT-URI: <redirect>
      GOOGLE-CLIENT_APPLICATION-NAME: <name>
```

### Running using Webpack server

Start the dependencies:

```
docker compose up -d year-planner year-planner-persistence
```

And

```
npm start
```

View the site at `http://localhost:8090/`.

### Running all on Docker compose

```
> docker compose up -d
```

You can then go to `http://localhost:8090/` to see the site. 

