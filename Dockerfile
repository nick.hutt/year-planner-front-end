FROM nginx:alpine

EXPOSE 80

COPY nginx/default.conf /etc/nginx/conf.d/

ENTRYPOINT exec nginx -g 'daemon off;'

COPY public /usr/share/nginx/html/
