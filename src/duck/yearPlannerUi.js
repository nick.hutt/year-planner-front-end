/**
 * Actions and reducers related to the year planner backend.
 * 
 * Packaged together as a Duck (Redux Modular Bundle)
 * For more information see: https://github.com/erikras/ducks-modular-redux
 */

export const VIEW_YEAR = "VIEW_YEAR";
export const VIEW_MONTH = "VIEW_MONTH";

const SELECT_YEAR = "SELECT_YEAR";
export const selectYear = (selectedYear) => {
    console.log(`Select year: ${selectedYear}`);
    return {
        type: SELECT_YEAR,
        selectedYear: selectedYear
    }
}

const SELECT_MONTH = "SELECT_MONTH";
export const selectMonth = (selectedYear, selectedMonth) => {
    console.log(`Select month: ${selectedYear}-${selectedMonth}`);
    return {
        type: SELECT_MONTH,
        selectedYear: selectedYear,
        selectedMonth: selectedMonth
    }
}

const SELECT_EVENT = "SELECT_EVENT";
export const selectEvent = (selectedEvent) => {
    console.log(`Event selected`, selectedEvent);
    return {
        type: SELECT_EVENT,
        selectedEvent: selectedEvent
    }
}

const DESELECT_EVENT = "DESELECT_EVENT";
export const deselectEvent = () => {
    console.log(`Event deselected`);
    return {
        type: DESELECT_EVENT
    }
}


const uiDefaultState = {
    view: VIEW_YEAR,
    selectedYear: undefined,
    selectedMonth: undefined,
    selectedEvent: undefined,
}

export default (state = uiDefaultState, action) => {
    console.log('UI reducer received: ' + action.type, action);

    switch(action.type) {
        case SELECT_YEAR:
            return {
                ...state,
                view: VIEW_YEAR,
                selectedYear: action.selectedYear,
                selectedMonth: undefined,
            };
        case SELECT_MONTH:
            return {
                ...state,
                view: VIEW_MONTH,
                selectedMonth: action.selectedMonth,
                selectedYear: action.selectedYear
            };
        case SELECT_EVENT:
            return {
                ...state,
                selectedEvent: action.selectedEvent
            };
        case DESELECT_EVENT:
            return {
                ...state,
                selectedEvent: undefined
            };
        default:
            return state;
    }
}