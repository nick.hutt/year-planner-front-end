/**
 * Actions and reducers related to the year planner backend.
 * 
 * Packaged together as a Duck (Redux Modular Bundle)
 * For more information see: https://github.com/erikras/ducks-modular-redux
 */
const REST_FAIL = "REST_FAIL";

const GET_USER_BY_USERNAME_REQUEST = "GET_USER_BY_USERNAME_REQUEST";
const GET_USER_BY_USERNAME_RECEIVE = "GET_USER_BY_USERNAME_RECEIVE";
const GET_USER_BY_USERNAME_FAIL = "GET_USER_BY_USERNAME_FAIL";
export const getUserByUsername = (username) => {
    console.log(`Get by username: ${username}`);
    return {
        types: [GET_USER_BY_USERNAME_REQUEST, GET_USER_BY_USERNAME_RECEIVE, GET_USER_BY_USERNAME_FAIL],
        payload: {
            request: {
                url: `/users/${username}`,
                method: 'GET'
            }
        }
    }
}

const GET_USER_BY_AUTH_CODE_REQUEST = "GET_USER_BY_AUTH_CODE_REQUEST";
const GET_USER_BY_AUTH_CODE_RECEIVE = "GET_USER_BY_AUTH_CODE_RECEIVE";
const GET_USER_BY_AUTH_CODE_FAIL = "GET_USER_BY_AUTH_CODE_FAIL";
export const getUserByAuthCode = (authCode) => {
    console.log(`Get by auth code: ${authCode}`);
    return {
        types: [GET_USER_BY_AUTH_CODE_REQUEST, GET_USER_BY_AUTH_CODE_RECEIVE, GET_USER_BY_AUTH_CODE_FAIL],
        payload: {
            request: {
                url: `/users/`,
                method: 'POST',
                data: {
                    'code': authCode
                }
            }
        }
    }
}

const GET_CALENDAR_REQUEST = "GET_CALENDAR_REQUEST";
const GET_CALENDAR_RECEIVE = "GET_CALENDAR_RECEIVE";
export const getCalendar = (userId, calendarId) => {
    console.log(`Getting calendar: ${calendarId} for userId: ${userId}`);
    return {
        types: [GET_CALENDAR_REQUEST, GET_CALENDAR_RECEIVE, REST_FAIL],
        payload: {
            request: {
                url: `/users/${userId}/calendars/${calendarId}`,
                method: 'GET'
            }
        }
    }
}

export const SIGN_OUT_PRESSED = "SIGN_OUT_PRESSED";
export const signOutPressed = () => {
    console.log(`Signing out`);
    return { type: SIGN_OUT_PRESSED };
}

export const GOOGLE_AUTH_PRESSED = "GOOGLE_AUTH_PRESSED";
export const googleAuthPressed = () => {
    console.log(`Google auth pressed`);
    return { type: GOOGLE_AUTH_PRESSED };
}


const restDefaultState = {
    loadingUser: false,
    user: undefined,
    errorMessage: undefined,
    loadingCalendar: false,
    calendar: undefined
}

export default (state = restDefaultState, action) => {
    console.log('Rest reducer received: ' + action.type, action);

    switch(action.type) {
        case GET_USER_BY_USERNAME_REQUEST:
        case GET_USER_BY_AUTH_CODE_REQUEST:
            return {
                ...state,
                user: undefined,
                loadingUser: true
            };
        case GET_USER_BY_USERNAME_RECEIVE:
        case GET_USER_BY_AUTH_CODE_RECEIVE:
            if (action.error) {
                return {
                    ...state,
                    loadingUser: false,
                    user: undefined,
                    errorMessage: 'Failed to load results'
                }
            } else {
                return {
                    ...state,
                    loadingUser: false,
                    user: action.payload.data
                };
            }
        case GET_CALENDAR_REQUEST:
            return {
                ...state,
                loadingCalendar: true,
                calendar: undefined
            };
        case GET_CALENDAR_RECEIVE:
            if (action.error) {
                return {
                    ...state,
                    loadingCalendar: false,
                    calendar: undefined
                };
            } else {
                return {
                    ...state,
                    loadingCalendar: false,
                    calendar: action.payload.data
                };
            }
        case SIGN_OUT_PRESSED:
            return {
                ...restDefaultState
            }
        case GOOGLE_AUTH_PRESSED:
            return {
                ...state,
                loadingUser: true
            }
        case GET_USER_BY_AUTH_CODE_FAIL:
            return {
                ...restDefaultState,
                authError: "Could not authenticate user"
            };
       case GET_USER_BY_USERNAME_FAIL:
            return {
                ...restDefaultState,
                authError: "Unable to find user"
            };
        case REST_FAIL:
            return {
                ...restDefaultState,
                authError: "An error occurred"
            };
        default:
            return state;
    }
}