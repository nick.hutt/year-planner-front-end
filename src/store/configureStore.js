import { compose, createStore, combineReducers, applyMiddleware } from 'redux';
import axiosMiddleware from 'redux-axios-middleware';
import yearPlannerRestReducer, { SIGN_OUT_PRESSED } from '../duck/yearPlannerRest';
import yearPlannerUiReducer from '../duck/yearPlannerUi';

import axios from 'axios';

export default () => {
    const client = axios.create({
        baseURL: process.env.YEAR_PLANNER_BACKEND_URL,
        responseType: 'json'
    });

    const appReducer = combineReducers({
        yearPlannerRest: yearPlannerRestReducer,
        yearPlannerUi: yearPlannerUiReducer,
    });

    const rootReducer = (state, action) => {
        if (action.type === SIGN_OUT_PRESSED) {
            state = undefined;
        }
      
        return appReducer(state, action);
    };

    const store = createStore(
        rootReducer,
        undefined,
        compose(
            applyMiddleware(axiosMiddleware(client))
        )
    )

    return store;
};

