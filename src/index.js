import React from 'react';
import {createRoot} from 'react-dom/client';
import { Provider } from 'react-redux';

import App from './components/App';

import configureStore from './store/configureStore';

const store = configureStore();
store.subscribe(() => {
    console.log("Store subscribe update event: ", store.getState());
})

// This is the ID of the div in your index.html file

const rootElement = document.getElementById('container');
const root = createRoot(rootElement);

root.render(
    <Provider store={store}>
        <App/>
    </Provider>);
