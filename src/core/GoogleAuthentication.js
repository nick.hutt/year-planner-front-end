export const checkPathForAuthCode = (getUserByAuthCode, loadCalendar) => {
    console.log("Checking for auth code...");
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get('code');
    if (!code) {
        console.log("No auth code found");
        return;
    }

    console.log("Auth code found");
    getUserByAuthCode(code).then(loadCalendar);
    window.history.pushState({}, document.title, "/");
}

export const redirectToGoogleAuth = () => {
    const newPage = "https://accounts.google.com/o/oauth2/v2/auth"
             + "?scope=" + encodeURIComponent("profile https://www.googleapis.com/auth/calendar.readonly")
             + "&access_type=offline"
             + "&include_granted_scopes=true"
             + "&state=state_parameter_passthrough_value"
             + "&redirect_uri=" + encodeURIComponent(process.env.GOOGLE_REDIRECT_URI)
             + "&response_type=code"
             + "&client_id=" + process.env.GOOGLE_CLIENT_ID;
    window.location.href = newPage;
}