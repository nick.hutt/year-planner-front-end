import moment from 'moment';
import { decorateYear, decorateMonth } from './Decorator';

export class YearPlanner {
    constructor(year) {
        this.year = year;
        this.months = [];
    }

    dates = () => {
        return this.months.map(month => month.dates)
                            .reduce((a, b) => a.concat(b), []);
    }

    sameYear = (matchYear) => {
        return this.year === matchYear; 
    }
}

export class Month {
    constructor(asMoment) {
        this.monthIndex = asMoment.month();
        this.monthName = asMoment.format("MMM");
        this.dates = [];
    }
}

export class MonthPlanner {
    constructor(year, month) {
        this.year = year;
        this.month = month;
        this.weeks = [];
    }

    dates = () => {
        return this.weeks.map(week => week.dates)
                            .reduce((a, b) => a.concat(b), []);
    }

    sameMonth = (matchYear, matchMonth) => {
        return this.year === matchYear && this.month === matchMonth; 
    }
}

export class Week {
    constructor(asMoment) {
        this.weekIndex = asMoment.isoWeek();
        this.dates = [];
    }
}

export class Date {
    constructor(asMoment) {
        this.asMoment = asMoment.clone();
        this.dayOfMonth = this.asMoment.format('D');
        this.dayOfWeek = this.asMoment.day();
        this.weekend = (this.dayOfWeek === 0 || this.dayOfWeek === 6);
        this.events = [];
    }

    format = (formatCode) => this.asMoment.format(formatCode);
}

export const createYearPlanner = (selectedYear, calendar) => {
    const yearPlanner = getUnpopulatedYear(selectedYear);
    populateDates(yearPlanner, calendar);
    decorateYear(yearPlanner);
    return yearPlanner;
}

const getUnpopulatedYear = (selectedYear) => {
    const yearPlanner = new YearPlanner(selectedYear);
    
    const dailyIterator = new moment({year: selectedYear}).startOf('year');
    while (dailyIterator.year() === yearPlanner.year) {
        const currentMonth = new Month(dailyIterator);
        yearPlanner.months.push(currentMonth);

        while (dailyIterator.month() == currentMonth.monthIndex) {
            const date = new Date(dailyIterator);
            currentMonth.dates.push(date);
    
            dailyIterator.add(1, 'days');
        }
    }

    return yearPlanner;
}

export const createMonthPlanner = (selectedYear, selectedMonth, calendar) => {
    const monthPlanner = getUnpopulatedMonth(selectedYear, selectedMonth);
    populateDates(monthPlanner, calendar);
    decorateMonth(monthPlanner);
    return monthPlanner;
}

const getUnpopulatedMonth = (selectedYear, selectedMonth) => {
    const monthPlanner = new MonthPlanner(selectedYear, selectedMonth);

    const endOfMonth = new moment({year: selectedYear, month: selectedMonth}).endOf('month');
    const dailyIterator = new moment({year: selectedYear, month: selectedMonth}).startOf('month').startOf('isoWeek');
    while (dailyIterator.isBefore(endOfMonth)) {
        const currentWeek = new Week(dailyIterator);
        monthPlanner.weeks.push(currentWeek);
        
        while (dailyIterator.isoWeek() === currentWeek.weekIndex) {
            const date = new Date(dailyIterator);
            date.belongsToSelectedMonth = dailyIterator.month() == selectedMonth;
            currentWeek.dates.push(date);

            dailyIterator.add(1, 'days');
        }
    }

    return monthPlanner;
}


const populateDates = (planner, calendar) => {
    calendar.events.forEach(event => addEventToPlanner(planner, event));
}

const addEventToPlanner = (planner, event) => {
    const eventStartDate = new moment(event.start.date ? event.start.date : event.start.datetime);
    // if (eventStartDate.year() > planner.year) {
    //     // early escape
    //     return;
    // }

    const eventEndDate = new moment(event.end.date ? event.end.date : event.end.datetime);
    // if (eventEndDate.year() < planner.year) {
    //     // early escape
    //     return;
    // }

    // console.log("Event occurs on the correct year", event);

    planner.dates()
        .filter(date => isDateWithinEventRange(eventStartDate, eventEndDate, date))
        .forEach(date => date.events.push(event));
}

const isDateWithinEventRange = (eventStartDate, eventEndDate, date) => {
    // essentially add to any day where the event lies in at all
    return date.asMoment.endOf('day').isSameOrAfter(eventStartDate)
            && date.asMoment.startOf('day').isSameOrBefore(eventEndDate);
}