
const WEEKEND_COLOR = '#dddddd';

const MONTH_COLORS = [
    "#858cd4",
    "#af85d4",
    "#d485d4",
    "#d485ad",
    "#d48589",
    "#d4b185",
    "#d4d385",
    "#afd485",
    "#85d489",
    "#85d4aa",
    "#85d4d3",
    "#85aed4"
]

export const decorateYear = (yearPlanner) => {
    yearPlanner.months.forEach((month, index) => month.color = MONTH_COLORS[index]);
    yearPlanner.dates()
                .filter(date => date.weekend)
                .forEach(date => date.color = WEEKEND_COLOR);
}

export const decorateMonth = (monthPlanner) => {
    monthPlanner.dates()
                .forEach(date => date.monthColor = MONTH_COLORS[date.asMoment.month()]);
    monthPlanner.dates()
                .filter(date => date.weekend)
                .forEach(date => date.color = WEEKEND_COLOR);
}