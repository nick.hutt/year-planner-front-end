import React from 'react';
import { connect } from 'react-redux';

import { checkPathForAuthCode, redirectToGoogleAuth } from '../core/GoogleAuthentication.js';

import { getUserByUsername, getUserByAuthCode, getCalendar, signOutPressed, googleAuthPressed } from '../duck/yearPlannerRest';
import { selectYear, selectMonth } from '../duck/yearPlannerUi';
import moment from 'moment';

import { ThemeProvider, StyledEngineProvider, createTheme } from '@mui/material/styles';
import { red, blue, blueGrey } from '@mui/material/colors';

import YearCalendar from './YearCalendar';
import MonthCalendar from './MonthCalendar';
import TopBar from './TopBar';
import SignIn from './SignIn';
import EventDialog from './EventDialog';

const theme = createTheme({
    typography: {
        useNextVariants: true,
     },
    palette: {
        primary: blueGrey,
        secondary: blue,
        error: red,
    },
});

class App extends React.Component {

    constructor(props) {
        super(props);
        this.topBarRef = React.createRef();
    }

    componentDidMount = () => {
        checkPathForAuthCode(this.props.getUserByAuthCode, this.loadCalendar);
    }

    signInPressed = (username) => {
        this.props.getUserByUsername(username).then(this.loadCalendar);
    }

    loadCalendar = () => {
        if (this.props.yearPlannerRest.user) {
            this.props.selectYear(new moment().startOf('year').year());
            this.props.getCalendar(this.props.yearPlannerRest.user.userId, this.props.yearPlannerRest.user.calendarIds[0])
        }
    }

    googleAuthPressed = () => {
        this.props.googleAuthPressed();
        redirectToGoogleAuth();
    }

    getTopBarHeight = () => {
        return this.topBarRef.current.clientHeight;
    }

    render() {
        return (
            <StyledEngineProvider injectFirst><ThemeProvider theme={theme}>
                    {this.props.yearPlannerRest.user &&
                        <div>
                            <div ref={this.topBarRef}>
                                <TopBar username={this.props.yearPlannerRest.user.username}
                                    signOutFunction={this.props.signOutPressed}
                                    selectedYear={this.props.yearPlannerUi.selectedYear}
                                    selectYearFunction={this.props.selectYear}
                                    selectedMonth={this.props.yearPlannerUi.selectedMonth}
                                    selectMonthFunction={this.props.selectMonth}
                                    view={this.props.yearPlannerUi.view}/>
                            </div>
                            {this.props.yearPlannerUi.view == 'VIEW_YEAR' && <YearCalendar/>}
                            {this.props.yearPlannerUi.view == 'VIEW_MONTH' && <MonthCalendar getTopBarHeightFunction={this.getTopBarHeight}/>}
                            <EventDialog/>
                        </div>
                    }
                    {!this.props.yearPlannerRest.user &&
                        <SignIn
                            signInFunction={this.signInPressed}
                            googleAuthFunction={this.googleAuthPressed}/>
                    }
                </ThemeProvider>
            </StyledEngineProvider>
        );
    }
}


function mapStateToProps(state) {
    return {
        yearPlannerRest: state.yearPlannerRest,
        yearPlannerUi: state.yearPlannerUi
    };
}

export default connect(mapStateToProps, {
    getUserByUsername: getUserByUsername,
    getUserByAuthCode: getUserByAuthCode,
    getCalendar: getCalendar,
    signOutPressed: signOutPressed,
    googleAuthPressed: googleAuthPressed,
    selectYear: selectYear,
    selectMonth: selectMonth
})(App);