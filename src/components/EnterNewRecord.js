import React from 'react';
import { connect } from 'react-redux';

import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import moment from 'moment';

import { enterNewRecord } from '../duck/yearPlannerRest';


class EnterNewRecord extends React.Component {
    constructor(props) {
        super(props);
        const now = moment().format('YYYY-MM-DDTHH:mm');
        this.state = {
            email: '',
            start: now,
            end: now
        }
    }

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    }

    formatDate = (input) => {
        // 28.11.2016 08:00
        return moment(input).format('DD.MM.YYYY HH:mm');
    }

    render() {
        return (
            (<Grid container justifyContent="center" spacing={24}>
                <Grid item xs={6}>
                    <Paper style={{ width: 500, padding: 20, margin: 20 }}>
                        <Typography variant="h3">
                            Enter new record
                        </Typography>
                        <TextField
                            id="name"
                            label="Email"
                            value={this.state.email}
                            onChange={this.handleChange('email')}
                            margin="normal"
                        />
                        <br/>
                        <TextField
                            id="start"
                            label="Start"
                            type="datetime-local"
                            value={this.state.start}
                            onChange={this.handleChange('start')}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <br />
                        <TextField
                            id="end"
                            label="End"
                            type="datetime-local"
                            value={this.state.end}
                            onChange={this.handleChange('end')}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <br />
                        <br />
                        <Button
                            onClick={() => this.props.enterNewRecord(this.state.email,
                                         this.formatDate(this.state.start),
                                         this.formatDate(this.state.end))}
                            variant="raised"
                            disabled={this.props.yearPlanner.enteringNewRecord || !this.state.email}>
                            Add record
                        </Button>
                        <br/>
                        {this.props.yearPlanner.enteredNewRecord &&
                            <Typography variant="body1" style={{marginTop: 10}}>
                                Record saved
                            </Typography>
                        }
                    </Paper>
                </Grid>
            </Grid>)
        );
    }
}

function mapStateToProps(state) {
    return {
        yearPlanner: state.yearPlanner
    };
}

export default connect(mapStateToProps, {
    enterNewRecord: enterNewRecord
})(EnterNewRecord);