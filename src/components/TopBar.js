import React from 'react';
import moment from 'moment';

import { styled } from "@mui/material/styles";

import { AppBar, Toolbar, Typography, IconButton, Button } from '@mui/material';
import { ChevronLeft, ChevronRight, PowerSettingsNew, SubdirectoryArrowLeft } from '@mui/icons-material';

const styles = {
    toolbarTitle: {
        display: 'block',
        marginRight: 30,
    },
    selectedYear: {

    },
    previous: {
        marginLeft: 2,
    },
    next: {

    },
    username: {
        flexGrow: 1,
        marginRight: 2,
        textAlign: 'right'
    }
};

const getSelectedPeriod = (currentMonthIndex, currentYear) => {
    if (currentMonthIndex) {
        return moment({ year: currentYear, month: currentMonthIndex });
    } else {
        return moment({ year: currentYear });
    }
}

const nextMonth = (selectMonthFunction, selectedPeriod) => {
    console.log("Selecting next month", selectedPeriod);
    const newPeriod = selectedPeriod.clone().add(1, 'months');
    selectMonthFunction(newPeriod.year(), newPeriod.month());
}

const previousMonth = (selectMonthFunction, selectedPeriod) => {
    console.log("Selecting previous month", selectedPeriod);
    const newPeriod = selectedPeriod.clone().subtract(1, 'months');
    selectMonthFunction(newPeriod.year(), newPeriod.month());
}

const TopBar = (props) => {
    const selectedPeriod = getSelectedPeriod(props.selectedMonth, props.selectedYear);
    return (
        <AppBar position="static" color="primary" sx={styles.appBar}>
            <Toolbar variant="dense">
                <Typography variant="body1" color="inherit" component="div">
                    Year Planner
                </Typography>
                {props.view == 'VIEW_YEAR' && 
                    <React.Fragment>
                        <IconButton aria-label="Previous Year" color="inherit" size="small" sx={styles.previous} onClick={() => props.selectYearFunction(props.selectedYear - 1)}>
                            <ChevronLeft/>
                        </IconButton>
                        <Typography variant="body1" color="inherit" sx={styles.selectedYear}>
                            {props.selectedYear}
                        </Typography>
                        <IconButton aria-label="Next Year" color="inherit" size="small" sx={styles.next} onClick={() => props.selectYearFunction(props.selectedYear + 1)} >
                            <ChevronRight/>
                        </IconButton>
                    </React.Fragment>
                }
                {props.view == 'VIEW_MONTH' &&
                    <React.Fragment>
                        <Button variant="text" size="medium" color="inherit" sx={styles.selectedYear} onClick={() => props.selectYearFunction(props.selectedYear)}>
                            {props.selectedYear}
                            <SubdirectoryArrowLeft/>
                        </Button>
                        <IconButton aria-label="Previous Month" color="inherit" size="small" sx={styles.previous} onClick={() => previousMonth(props.selectMonthFunction, selectedPeriod)}>
                            <ChevronLeft/>
                        </IconButton>
                        <Typography variant="body1" color="inherit" sx={styles.selectedMonth}>
                            {selectedPeriod.format("MMM YYYY")}
                        </Typography>
                        <IconButton aria-label="Next Month" color="inherit" size="small" sx={styles.next} onClick={() => nextMonth(props.selectMonthFunction, selectedPeriod)} >
                            <ChevronRight/>
                        </IconButton>
                    </React.Fragment>
                }
                <Typography variant="body1" color="inherit" sx={styles.username}>
                    {props.username}
                </Typography>
                <IconButton variant="outlined" color="inherit" onClick={props.signOutFunction} size="small">
                    <PowerSettingsNew/>
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}

export default styled(TopBar)();

