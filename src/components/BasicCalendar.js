import React from 'react';
import { connect } from 'react-redux';

import { Typography, Paper, Grid, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import moment from 'moment';

const formatTemporalPoint = (input) => {
    if (input.date) {
        return moment(input.date).format('D MMM');
    } else if (input.datetime) {
        return moment(input.datetime).format('D MMM HH:mm');
    } else {
        return "Unexpected date";
    }
}

class BasicCalendar extends React.Component {

    render() {
        if (!this.props.yearPlanner.user || !this.props.yearPlanner.calendar) {
            return "";
        }

        return (
            (<Grid container justifyContent="center" spacing={24}>
                <Grid item xs={6}>
                    <Paper style={{ width: 500, padding: 20, margin: 20 }}>
                        <Typography variant="h6" gutterBottom>
                            Basic Calendar
                        </Typography>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Description</TableCell>
                                    <TableCell align="right">Start date</TableCell>
                                    <TableCell align="right">Finish date</TableCell>
                                </TableRow>
                            </TableHead>
                            {this.props.yearPlanner.calendar &&
                                <TableBody>
                                    {this.props.yearPlanner.calendar.events.map((entry, index) => {
                                        return (
                                            <TableRow key={index}>
                                                <TableCell>{entry.title}</TableCell>
                                                <TableCell align="right">{formatTemporalPoint(entry.start)}</TableCell>
                                                <TableCell align="right">{formatTemporalPoint(entry.end)}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            }
                        </Table>
                    </Paper>
                </Grid>
            </Grid>)
        );
    }
}

function mapStateToProps(state) {
    return {
        yearPlanner: state.yearPlanner
    };
}

export default connect(mapStateToProps)(BasicCalendar);