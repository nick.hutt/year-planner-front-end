// Based on Material example
// https://github.com/mui-org/material-ui/tree/master/docs/src/pages/getting-started/page-layout-examples/sign-in

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Avatar, Button, CssBaseline, FormControl, Input, InputLabel, Paper, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { Container, styled } from "@mui/system";

const styles = {
    main: {
        display: 'block', // Fix IE 11 issue.
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    paper: {
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 3,
    },
    avatar: {
        margin: 1,
        backgroundColor: 'blue',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: 1,
    },
    submit: {
        marginTop: 3,
    },
    googleSignIn: {
        marginTop: 3,
        backgroundColor: '#4285F4',
    },
    error: {
        color: 'red',
        marginTop: 1,
    }
};

class SignIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: ''
        }
    }

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    }

    render() {
        return (
            <Container sx={styles.main}>
                <CssBaseline />
                <Paper sx={styles.paper}>
                    <Typography component="h1" variant="h4">
                        Year Planner
                    </Typography>
                    <Avatar sx={styles.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign In
                    </Typography>
                    <FormControl margin="normal" required fullWidth sx={styles.form}>
                        <InputLabel htmlFor="username">Username</InputLabel>
                        <Input
                            id="username"
                            name="username"
                            onChange={this.handleChange('username')}
                            autoComplete="username"
                            onKeyPress={(e) => {
                                if (e.which === 13) {
                                    this.props.signInFunction(this.state.username)}
                                }
                            }
                            autoFocus />
                    </FormControl>
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        sx={styles.submit}
                        onClick={() => {this.props.signInFunction(this.state.username)}}
                        disabled={this.props.yearPlannerRest.loadingUser || !this.state.username}
                    >
                        Sign in
                    </Button>
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        sx={styles.googleSignIn}
                        onClick={this.props.googleAuthFunction}
                        disabled={this.props.yearPlannerRest.loadingUser}
                    >
                        Sign in with Google Calendar
                    </Button>
                    {this.props.yearPlannerRest.authError &&
                        <Typography variant="body1" sx={styles.error}>
                            {this.props.yearPlannerRest.authError}
                        </Typography>
                    }
                </Paper>
            </Container>
        );
    }
}

SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    return {
        yearPlannerRest: state.yearPlannerRest
    };
}

export default connect(mapStateToProps)(styled(SignIn)());