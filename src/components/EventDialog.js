import React from 'react';
import { connect } from 'react-redux';
import { deselectEvent } from '../duck/yearPlannerUi';

import { styled } from "@mui/material/styles";

import { TextField, Button, IconButton, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { Brightness1 } from '@mui/icons-material';

const styles = {
    title: {
        marginBottom: 0,
        paddingBottom: 0
    },
    endInput: {
        marginLeft: 20,
    },
    color: {
        paddingTop: 20,
    },
    colorDrop: {
        marginLeft: 20,
    }
};

class EventDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }
  
    handleClose = () => {
        this.props.deselectEvent();
    }

    render() {
        if (!this.props.yearPlannerUi.selectedEvent) {
            return "";
        }

        const event = this.props.yearPlannerUi.selectedEvent;

        return (
            (<div>
                <Dialog open={true} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title" sx={styles.title}>{event.title}</DialogTitle>
                    <DialogContent>
                        <TextField
                            margin="normal"
                            id="name"
                            label="Title"
                            value={event.title}
                            fullWidth
                            InputProps={{
                                readOnly: true,
                            }}
                            />
                        <TextField
                            label="Description"
                            multiline
                            maxRows="4"
                            value={event.description || ""}
                            margin="normal"
                            fullWidth
                            InputProps={{
                                readOnly: true,
                            }}
                        />
                        {event.start.datetime &&
                            <TextField
                                label="Starts at"
                                type="datetime-local"
                                value={event.start.datetime.substring(0, 19)}
                                margin="normal"
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        }
                        {event.start.date &&
                            <TextField
                                label="Starts at"
                                type="date"
                                value={event.start.date}
                                margin="normal"
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        }
                        {event.end.datetime &&
                            <TextField
                                label="Ends at"
                                type="datetime-local"
                                value={event.end.datetime.substring(0, 19)}
                                margin="normal"
                                sx={styles.endInput}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        }
                        {event.end.date &&
                            <TextField
                                label="Ends at"
                                type="date"
                                value={event.end.date}
                                margin="normal"
                                sx={styles.endInput}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        }
                        <div style={styles.color}>
                            Color:
                            <IconButton color="inherit" size="small" style={{ color: event.color }} sx={styles.colorDrop}>
                                <Brightness1/>
                            </IconButton>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>)
        );
    }
}

function mapStateToProps(state) {
    return {
        yearPlannerUi: state.yearPlannerUi
    };
}

export default connect(mapStateToProps, {
    deselectEvent: deselectEvent
})(styled(EventDialog)());