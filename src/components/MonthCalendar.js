import React from 'react';
import { connect } from 'react-redux';
import { selectEvent } from '../duck/yearPlannerUi';

import { Card, Paper, Grid } from '@mui/material';
import { createMonthPlanner } from '../core/Planner';
import { styled } from "@mui/material/styles";

const styles = {
    container: {
        background: '#f8f8f8',
    },
    titleRow: {
        marginTop: 1,
    },
    weekGridRow: {
        marginBottom: 1,
    },
    titlePaper: {
        fontSize: 12,
        fontWeight: 600,
        width: '100%',
        paddingTop: 0,
        paddingBottom: 0,
        alignItems: 'center',
        textAlign: 'center',
    },
    dateCard: {
        display: 'flex',
        height: '100%',
        marginBottom: 0.5,
        marginRight: 0.5,
        marginLeft: 0.5,
    },
    adjacentMonthDateCard: {
        opacity: 0.35,
    },
    dayOfMonth: {
        display: 'flex',
        flexDirection: 'column',
        width: 14,
        paddingLeft: 2,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 2,
        fontSize: 10,
        alignItems: 'center',
        fontWeight: 600,
    },
    dayOfAdjacentMonth: {
        fontWeight: 400,
    },
    daySlot: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'left',
        width: '100%',
    },
    eventCard: {
        paddingLeft: 0.2,
        paddingRight: 0.2,
        paddingTop: 0.2,
        paddingBottom: 0.2,
        margin: 0.2,
        fontSize: 10,
        height: 8 + 2 + 1,
        cursor: 'pointer',
    },
};

const TITLE_ROW_HEIGHT = 20;

class MonthCalendar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }
    
    updateWindowDimensions = () => {
        // FIXME minus 60 just a buffer for now
        const height = window.innerHeight - this.props.getTopBarHeightFunction() - 60;
        console.log("Available height now", height);
        this.setState({ width: window.innerWidth, height: height });
    }

    componentDidMount = () => {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        this.buildCalendar();
    }

    componentDidUpdate = () => {
        this.buildCalendar();
    }
    
    componentWillUnmount = () => {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
  
    buildCalendar = () => {
        if (!this.props.yearPlannerRest.calendar) {
            return;
        }

        if (!this.state.monthPlanner || 
                !this.state.monthPlanner.sameMonth(this.props.yearPlannerUi.selectedYear, this.props.yearPlannerUi.selectedMonth)) {
            const monthPlanner = createMonthPlanner(this.props.yearPlannerUi.selectedYear,
                                            this.props.yearPlannerUi.selectedMonth, 
                                            this.props.yearPlannerRest.calendar);
            this.setState({ monthPlanner: monthPlanner });
        }
    }

    getTitleRowHeight = () => {
        return TITLE_ROW_HEIGHT + "px";
    }

    getWeekRowHeight = () => {
        if (!this.state.monthPlanner) {
            return 0;
        }

        const availableSpace = this.state.height - TITLE_ROW_HEIGHT;
        const pixelHeightPerRow = availableSpace / this.state.monthPlanner.weeks.length;
        return pixelHeightPerRow + "px";
    }
   

    render() {
        if (!this.state.monthPlanner) {
            return "";
        }

        return (
            <div style={styles.container}>
                {this.state.monthPlanner.weeks.map((week, i) => (
                    <React.Fragment key={i}>
                        {i == 0 && (
                            <Grid container spacing={0} key={-1} sx={styles.titleRow} style={{ height: this.getTitleRowHeight()}}>
                                {week.dates.map((date, j) => (
                                    <Grid item xs key={j}>
                                        <Paper square={true} sx={styles.titlePaper} elevation={0}>
                                            {date.format("dddd")}
                                        </Paper>
                                    </Grid>
                                ))}
                            </Grid>
                        )}
                        <Grid container spacing={0} key={i} sx={styles.weekGridRow} style={{ height: this.getWeekRowHeight()}}>
                            {week.dates.map((date, j) => (
                                <Grid item key={j} xs>
                                    <Card sx={date.belongsToSelectedMonth ? styles.dateCard : {...styles.dateCard, ...styles.adjacentMonthDateCard}} raised={date.belongsToSelectedMonth}>
                                        <div style={date.belongsToSelectedMonth ? {...styles.dayOfMonth, backgroundColor: date.monthColor} : {...styles.dayOfMonth, ...styles.dayOfAdjacentMonth, backgroundColor: date.monthColor}}>
                                            {date.dayOfMonth}
                                        </div>
                                        <div style={{...styles.daySlot, backgroundColor: date.color}}>
                                            {date.events.map((event, j) => (
                                                <Card raised={true} sx={styles.eventCard}
                                                        key={j} 
                                                        style={{background: `linear-gradient(90deg, #ffffff 0%, ${event.color} 200%)`}}
                                                        onClick={() => this.props.selectEvent(event)}>
                                                    {event.title}
                                                </Card>
                                            ))}
                                        </div>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                    </React.Fragment>
                ))}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        yearPlannerRest: state.yearPlannerRest,
        yearPlannerUi: state.yearPlannerUi
    };
}

export default connect(mapStateToProps, {
    selectEvent: selectEvent
})(styled(MonthCalendar)());