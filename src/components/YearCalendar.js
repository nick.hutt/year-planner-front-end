import React from 'react';
import { connect } from 'react-redux';
import { selectEvent, selectMonth } from '../duck/yearPlannerUi';

import { Card, Paper } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { createYearPlanner } from '../core/Planner';
import { styled } from "@mui/material/styles";

const styles = {
    monthTitleCard: {
        fontSize: 10,
        fontWeight: 800,
        textAlign: 'center',
        width: '100%',
        paddingTop: 0.4,
        paddingBottom: 0.4,
        cursor: 'pointer',
    },
    dateCard: {
        display: 'flex',
    },
    dayOfMonth: {
        display: 'flex',
        flexDirection: 'column',
        width: 16,
        paddingLeft: 2,
        paddingTop: 2,
        paddingBottom: 2,
        fontSize: 8,
        alignItems: 'center',
    },
    dayName: {
        display: 'flex',
        flexDirection: 'column',
        width: 20,
        paddingLeft: 1,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 5,
        fontSize: 8,
        alignItems: 'center',
    },
    daySlot: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'left',
        width: '100%',
    },
    eventCard: {
        paddingLeft: 0.2,
        paddingRight: 0.2,
        paddingTop: 0.2,
        paddingBottom: 0.2,
        margin: 0.2,
        fontSize: 8,
        height: 8 + 2 + 1,
        cursor: 'pointer',
    },
};

class YearCalendar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount = () => {
        this.buildCalendar();
    }

    componentDidUpdate = () => {
        this.buildCalendar();
    }

    buildCalendar = () => {
        if (!this.props.yearPlannerRest.calendar) {
            // calendar hasn't loaded
            return;
        }

        if (!this.state.yearPlanner || !this.state.yearPlanner.sameYear(this.props.yearPlannerUi.selectedYear)) {
            const yearPlanner = createYearPlanner(this.props.yearPlannerUi.selectedYear, this.props.yearPlannerRest.calendar);
            this.setState({ yearPlanner: yearPlanner });
        }
    }
   

    render() {
        if (!this.state.yearPlanner) {
            return "";
        }

        return (
            (<Grid container justifyContent="center" spacing={0}>
                {this.state.yearPlanner.months.map((month, i) => (
                    <Grid size={1} key={i} item xs={1}>
                        <Paper square={true}>
                            <Card sx={styles.monthTitleCard} 
                                    style={month.color && { backgroundColor: month.color}}
                                    onClick={() => this.props.selectMonth(this.state.yearPlanner.year, month.monthIndex)}>
                                {month.monthName}
                            </Card>
                        </Paper>
                        {month.dates.map((date, j) => (
                            <Card sx={styles.dateCard} key={j}>
                                <div style={{...styles.dayOfMonth, backgroundColor: month.color}}>
                                    {date.dayOfMonth}
                                </div>
                                <div style={{...styles.dayName, backgroundColor: month.color}}>
                                    {date.format("dd")}
                                </div>
                                <div style={{...styles.daySlot, backgroundColor: date.color}}>
                                    {date.events.map((event, j) => (
                                        <Card raised={true} sx={styles.eventCard} key={j} 
                                                style={{background: `linear-gradient(90deg, #ffffff 0%, ${event.color} 200%)`}}
                                                onClick={() => this.props.selectEvent(event)}>
                                            {event.title}
                                        </Card>
                                    ))}
                                </div>
                            </Card>
                        ))}
                    </Grid>
                ))}
            </Grid>)
        );
    }
}

function mapStateToProps(state) {
    return {
        yearPlannerRest: state.yearPlannerRest,
        yearPlannerUi: state.yearPlannerUi
    };
}

export default connect(mapStateToProps, {
    selectEvent: selectEvent,
    selectMonth: selectMonth
})(styled(YearCalendar)());